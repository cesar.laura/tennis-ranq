<?php

namespace App\Helpers;

use App\Models\Match;
use App\Models\Result;  
use App\Models\User;
use App\Models\Variable;

class OptionsHelper{
    /**
     * Private constructor, `new` is disallowed by design.
     */
    private function __construct(){ }
    public static function months(){
        return [
            "01" => "Enero", 
            "02" => "Febrero", 
            "03" => "Marzo", 
            "04" => "Abril", 
            "05" => "Mayo", 
            "06" => "Junio", 
            "07" => "Julio", 
            "08" => "Agosto", 
            "09" => "Setiembre", 
            "10" => "Octubre", 
            "11" => "Noviembre", 
            "12" => "Diciembre" 
        ];
    }
    public static function Calc(
        Match &$match
    ){
        $result = new Result;
        $result->match_id = $match->id;
    
        $variable = Variable::find(1);
    
        $player1 = User::find($match->player1_id);
        $player2 = User::find($match->player2_id);
        
        // points_lost_in_sets && points_won_in_sets
    
        $firstPlayerPoints = 0; // For set
        $secondPlayerPoints = 0; // For set
    
        $numberOfSetsWonPlayer1 = 0;
        $numberOfSetsWonPlayer2 = 0;
    
        foreach ($match->sets as $set) {
            $firstPlayerPoints += $set->player1;
            $secondPlayerPoints += $set->player2;
            $set->player1 > $set->player2 ?
                $numberOfSetsWonPlayer1 ++ :
                $numberOfSetsWonPlayer2 ++;
        }
        $player1->information->points_lost_in_sets += $secondPlayerPoints;
        $player1->information->points_won_in_sets += $firstPlayerPoints;
    
        $player2->information->points_lost_in_sets += $firstPlayerPoints;
        $player2->information->points_won_in_sets += $secondPlayerPoints;
    
        // r1 && r2
        $r1 = 0; // For player 1
        $r2 = 0; // For player 2
    
        if($numberOfSetsWonPlayer1 > $numberOfSetsWonPlayer2){
            $r1 = 1; // Player 1 winner
    
            $result->number_of_sets_won = $numberOfSetsWonPlayer1;
            $result->number_of_lost_sets = $numberOfSetsWonPlayer2;
        }else{
            $r2 = 1; // Player 2 winner
    
            $result->number_of_sets_won = $numberOfSetsWonPlayer2;
            $result->number_of_lost_sets = $numberOfSetsWonPlayer1;
        }
        
        // For player 1
        $dp = $match->player1->information->points - $match->player2->information->points;
        $points1 = $variable->B1 * ($r1 - (1 / (pow(10, -$dp / $variable->B2) + 1)));
    
        // For player 2
        $dp = $match->player2->information->points - $match->player1->information->points;
        $points2 = $variable->B1 * ($r2 - (1 / (pow(10, -$dp / $variable->B2) + 1)));
    
        $points1 += $variable->B3;      $points2 += $variable->B3;
    
        // winner && loser
        if($r1){
            /* Player 1 winner */
            $result->pts_won = $points1;     $result->lost_pts = $points2;
    
            $result->number_of_sets_won = $numberOfSetsWonPlayer1;
            $result->number_of_lost_sets = $numberOfSetsWonPlayer2;
    
            $player1->information->number_of_wins ++;
            $player2->information->number_of_defeats ++;
    
            $result->winner = $player1->email;
            $result->loser = $player2->email;
        }else{
            /* Player 2 winner */
            $result->pts_won = $points2;     $result->lost_pts = $points1;
    
            $result->number_of_sets_won = $numberOfSetsWonPlayer2;
            $result->number_of_lost_sets = $numberOfSetsWonPlayer1;
    
            $player1->information->number_of_defeats ++;
            $player2->information->number_of_wins ++;
            $result->winner = $player2->email;
            $result->loser = $player1->email;
        }
    
        $result->save();
        $player1->information->update();
        $player2->information->update();    
    
        $match->status = 1;
        $match->update();
    }   
}