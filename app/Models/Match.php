<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    use HasFactory;

    // Relations

    public function player1()
    {
        return $this->belongsTo(User::class, 'player1_id');
    }
    public function player2()
    {
        return $this->belongsTo(User::class, 'player2_id');
    }
    public function sets()
    {
        return $this->hasMany(Set::class);
    }
    public function result(){
        return $this->hasOne(Result::class);
    }
}
