<?php

namespace App\Http\Controllers;

use App\Helpers\OptionsHelper;
use App\Models\Information;
use App\Models\Match;
use App\Models\Result;
use App\Models\User;
use App\Models\Variable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){}
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $informations = Information::join('users', 'users.id', 'information.id')
            ->where('users.status', 1)
            ->orderBy('points', 'desc')
            ->get();
        return view('index', compact('informations'));
    }
    public function criterion(){return view('criterion');}
    public function championship(){return view('championship');}
    public function statistics(){
        $informations = Information::join('users', 'users.id', 'information.id')
            ->where('users.status', 1)
            ->orderBy('points', 'desc')
            ->get();
        return view('statistics', compact('informations'));
    }
    public function matches(){
        $matches = Match::where('status', 2)->orderBy('id', 'ASC')->get();
        $partyGroups = [];
        $months = OptionsHelper::months();
        foreach ($matches as $match) {
            $partyGroups["{$match->upper_limit}{$match->lower_limit}"][] = $match; 
        }
        return view('matches', compact('partyGroups', 'months'));
    }
    public function gallery(){return view('gallery');}
    public function updateRanking2(){
        $matches = Match::where('status', 1)->get();
        $pointsToIncrease = [];
        if(count($matches)){
            $pointsToIncrease = $this->createArray($matches); 
            foreach ($matches as $match) {
                $player1 = User::find($match->player1_id);
                $player2 = User::find($match->player2_id);
                if($player1->email == $match->result->winner){
                    // Player 1 winner
                    $pointsToIncrease["$player1->id"][0] += $match->result->pts_won;
                    $pointsToIncrease["$player2->id"][0] += $match->result->lost_pts;
                }else{
                    // Player 2 winner
                    $pointsToIncrease["$player2->id"][0] += $match->result->pts_won;
                    $pointsToIncrease["$player1->id"][0] += $match->result->lost_pts;
                }
                $match->upper_limit = Carbon::now()->toDateString();
                $variable = Variable::find(1);
                $variable->temporary_date = Carbon::now()->addDay()->toDateString();
                $variable->update();
                $pointsToIncrease["$player1->id"][1] ++;
                $pointsToIncrease["$player2->id"][1] ++;
                $match->status = 2;
                $match->update();
            }
            $this->updatePoints($pointsToIncrease);
        }
        return redirect('/')->with('info', 'Operación exitosa :');
    }
    public function createArray(&$matches){
        foreach($matches as $match){
            $arr["$match->player1_id"][0] = 0;
            $arr["$match->player1_id"][1] = 0;
            $arr["$match->player2_id"][0] = 0;
            $arr["$match->player2_id"][1] = 0;
        }
        return $arr;
    }
    public function calc($x, $y, $r, $pointsPrev, $pointsPrev2){
        $dp = $pointsPrev - $pointsPrev2;
        $points = $x * ($r - (1 / (pow(10, -$dp / $y) + 1)));
        return round($points);
    }
    public function updatePoints(&$pointsToIncrease){
        foreach ($pointsToIncrease as $id => $_) {
            $information = Information::find($id);
            $value = $pointsToIncrease[$id][0] / $pointsToIncrease[$id][1];
            $information->points += $value;
            $information->last_points_won = $value;
            $information->update();
        }
    }

    public function confirmMatchScores($token, $id){
        $match = Match::find($id);
        if($match->token == $token){
            OptionsHelper::Calc($match);
            return redirect('/')->with('info', 'Operación exitosa');
        }
        return redirect('/')->with('info', '::::::::::::::::> La Operación no se pudo dar');
    }
}
