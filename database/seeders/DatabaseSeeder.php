<?php

namespace Database\Seeders;

use App\Models\Information;
use App\Models\Match;
use App\Models\Result;
use App\Models\Set;
use App\Models\User;
use App\Models\Variable;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(5)->create()->each(function ($user){
            $information = new Information();
            $information->user_id = $user->id;
            $information->points = 200;
            $information->number_of_wins = 0;
            $information->number_of_defeats = 0;
            $information->save();
        });

        $user = User::create([
            'names' => 'Derry Nikolai',
            'surnames' => 'Sucari Aduviri',
            'email' => 'dsucaria@ulasalle.edu.pe',
            'status' => 1,
            'password' => Hash::make('xhtirakus')
        ]);

        $information = new Information();
        $information->user_id = $user->id;
        $information->points = 270;
        $information->save();
        
        $user = User::create([
            'names' => 'Sebastián Octavio',
            'surnames' => 'Manrique Puma',
            'email' => 'sebas@gmail.com',
            'status' => 1,
            'password' => Hash::make('xhtirakus')
        ]);

        $information = new Information();
        $information->user_id = $user->id;
        $information->points = 230;
        $information->save();

        Variable::create([
            'B1' => 25,
            'B2' => 300,
            'B3' => 2
        ]);
    }
}
