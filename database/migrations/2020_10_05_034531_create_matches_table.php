<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('player1_id')->unsigned();
            $table->bigInteger('player2_id')->unsigned();
            $table->string('date');
            $table->string('upper_limit')->nullable();
            $table->string('lower_limit');
            $table->string('type')->default('D');
            $table->integer('number_of_sets');
            $table->integer('status');
            $table->string('token')->nullable();

            $table->timestamps();

            $table->foreign('player1_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('player2_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
