<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('match_id')->unsigned();
            $table->string('winner');
            $table->string('loser');
            $table->integer('pts_won');
            $table->integer('lost_pts');
            $table->integer('number_of_sets_won');
            $table->integer('number_of_lost_sets');
            
            $table->timestamps();
            
            $table->foreign('match_id')
                ->references('id')
                ->on('matches')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
