<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;


Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('criterion', [HomeController::class, 'criterion'])->name('criterion');
Route::get('championship', [HomeController::class, 'championship'])->name('championship');

/* Route::get('ranking', [HomeController::class, 'ranking'])->name('ranking'); */
Route::get('statistics', [HomeController::class, 'statistics'])->name('statistics');
Route::get('matches', [HomeController::class, 'matches'])->name('matches');
Route::get('gallery', [HomeController::class, 'gallery'])->name('gallery');

Route::get('user', [UserController::class, 'index']);

Route::get('user/matches', [UserController::class, 'matches'])->name('user.matches');
Route::get('user/matchesCreated', [UserController::class, 'matchesCreated'])->name('user.matchesCreated');
Route::get('user/unapprovedMatches', [UserController::class, 'unapprovedMatches'])->name('user.unapprovedMatches');

Route::get('user/createMatch', [UserController::class, 'createMatch'])->name('user.createMatch');
Route::post('createMatch', [UserController::class, 'store'])->name('user.store');

Route::put('user/{id}/editMatch', [UserController::class, 'update'])->name('user.update');
Route::get('user/{id}/editMatch', [UserController::class, 'editMatch'])->name('user.editMatch');

Route::delete('user/{id}/deleteMatch', [UserController::class, 'deleteMatch'])->name('user.deleteMatch');

// Route::resource('user', UserController::class);

// This is the route
Route::get('updateRanking/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZjY3ODU4Nzg3NDBkOGE2ZjVlNzBiMWIiLCJhcGVsbGlkb3MiOiJTdWNhcmkiLCJiaW9ncmFmaWEiOiIiLCJlbWFpbCI6ImRzdWNhcmlhQHVsYXNhbGxlLmVkdS5wZSIsImV4cCI6MTYwMTQxMjU2NywiZmVjaGFfbmFjaW1pZW50byI6IjIwMDEtMTItMTVUMDA6MDA6MDBaIiwibm9tYnJlIjoiRGVycnkiLCJzaXRpV2ViIjoiIiwidWJpY2FjaW9uIjoiIn0.425WrCA9pd9Dln8BoHRx8YTQY3m9SUAnkLlU6U6G2QE', [HomeController::class, 'updateRanking2']);
Route::get('confirmMatchScores/{token}/{id}', [HomeController::class, 'confirmMatchScores'])->name('home.confirmMatchScores');

Auth::routes();
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// updateRanking/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZjY3ODU4Nzg3NDBkOGE2ZjVlNzBiMWIiLCJhcGVsbGlkb3MiOiJTdWNhcmkiLCJiaW9ncmFmaWEiOiIiLCJlbWFpbCI6ImRzdWNhcmlhQHVsYXNhbGxlLmVkdS5wZSIsImV4cCI6MTYwMTQxMjU2NywiZmVjaGFfbmFjaW1pZW50byI6IjIwMDEtMTItMTVUMDA6MDA6MDBaIiwibm9tYnJlIjoiRGVycnkiLCJzaXRpV2ViIjoiIiwidWJpY2FjaW9uIjoiIn0.425WrCA9pd9Dln8BoHRx8YTQY3m9SUAnkLlU6U6G2QE