@extends('layouts.app')

@section('content')
<div class="limiter full-matches">


    <div class="search-boxen">

        @foreach ($partyGroups as $matches)
        <h1>
            @if ($months["{$matches[0]->lower_limit[5]}{$matches[0]->lower_limit[6]}"] == $months["{$matches[0]->upper_limit[5]}{$matches[0]->upper_limit[6]}"])
            {{$months["{$matches[0]->lower_limit[5]}{$matches[0]->lower_limit[6]}"]}}
            {{"{$matches[0]->lower_limit[8]}{$matches[0]->lower_limit[9]}"}}
            a {{"{$matches[0]->lower_limit[8]}{$matches[0]->lower_limit[9]}" + 6}}
            @else
            {{$months["{$matches[0]->lower_limit[5]}{$matches[0]->lower_limit[6]}"]}}
            {{"{$matches[0]->lower_limit[8]}{$matches[0]->lower_limit[9]}"}}
            a {{$months["{$matches[0]->upper_limit[5]}{$matches[0]->upper_limit[6]}"]}}
            {{"{$matches[0]->lower_limit[8]}{$matches[0]->lower_limit[9]}" + 6}}
            @endif
        </h1>
        <div class="search-result">
            @foreach ($matches as $match)
            <div class="benchmarks table">
                <ul class="table-body">
                    <li class="tr">
                        <span class="metric td" style="width: 6%;">
                            {{$match->player1->names}} <label>{{$match->player1->surnames}}</label>
                        </span>
                        @foreach ($match->sets as $set)
                        <span class="td percentile-76"><span class="number">{{$set->player1}}</span></span>
                        @endforeach
                        {{-- points --}}
                        <span class="td percentile-76">
                            <span class="number pointsWL">
                                @if ($match->result->winner == $match->player1->email) +{{ $match->result->pts_won }}
                                @else {{ $match->result->lost_pts }}
                                @endif
                            </span>
                        </span>
                        {{-- end points --}}
                    </li>
                    <li class="tr">
                        <span class="metric td">
                            {{$match->player2->names}} <label>{{$match->player2->surnames}}</label>
                        </span>
                        @foreach ($match->sets as $set)
                        <span class="td percentile-76"><span class="number">{{$set->player2}}</span></span>
                        @endforeach
                        {{-- points --}}
                        <span class="td percentile-76">
                            <span class="number pointsWL">
                                @if ($match->result->winner == $match->player2->email) +{{ $match->result->pts_won }}
                                @else {{ $match->result->lost_pts }}
                                @endif
                            </span>
                        </span>
                        {{-- end points --}}
                    </li>
                </ul>
                <ul class="table-footer">
                    <li class="th">
                    </li>
                    <li class="th"><label>{{$match->date}}</label></li>
                    <li class="th"></li>
                </ul>
            </div>
            @endforeach


        </div>
        @endforeach
    </div>



</div>
@endsection