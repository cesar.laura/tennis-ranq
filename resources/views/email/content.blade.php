<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tennis</title>
    <style>
        .table-responsive {
            min-height: .01%;
            overflow-x: auto;
        }
        td{
            text-align: center
        }
        @media screen and (max-width: 767px) {
            .table-responsive {
                width: 100%;
                margin-bottom: 15px;
                overflow-y: hidden;
                -ms-overflow-style: -ms-autohiding-scrollbar;
                border: 1px solid #ddd;
            }
            .table-responsive > .table {
                margin-bottom: 0;
            }
            .table-responsive > .table > thead > tr > th,
            .table-responsive > .table > tbody > tr > th,
            .table-responsive > .table > thead > tr > td,
            .table-responsive > .table > tbody > tr > td,{
                white-space: nowrap;
            }
        }
    </style>
</head>
<body>
    <div class="table-responsive">
        <p><a href="http://127.0.0.1:8000/confirmMatchScores/{{$match->token}}/{{$match->id}}">CONFIRMAR</a></p>
        <table style="width:100%;">
            <thead>
                <tr>
                    <th>#SET</th>
                    <th>Jugador1</th>
                    <th>Tú</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($match->sets as $set)
                    <tr>
                        <td>{{$set->number}}</td>
                        <td>{{$set->player1}}</td>
                        <td>{{$set->player2}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</html>
