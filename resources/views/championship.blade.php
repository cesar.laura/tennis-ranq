@extends('layouts.app')

@section('content')
<div class="limiter">

    <div class="header-gallery">
        <h1>Campeonatos</h1>
        <p>1er. Campeonato 2019</p>
    </div>
    <div class="evidencia-campeonato">
  
      <img src="{{ asset('images/campeonato.png') }}" style="width:90%">

    </div>
    <div class="bottom-champion">
        <h1>INFORMACIÓN GENERAL</h1>
        <p>Fecha : Sábado 19 - Domingo 20 y Sábado 26 de Octubre</p>
        <p> ​​Costo : 50 Soles (Parrilla compartida para todos los participantes)
            Modalidad : Singles (8 grupos de 4 jugadores) Cada grupo juegan todos contra todos. Cladifican 2 jugadores de cada grupo a los 8vos. de final
            Partidos : Los partidos de grupos son a 1 set con punto decisivo. Los demás partidos al mejor de 2 sets con Tie Break en el tercer set
            Puntos Ranking : Los indicados en la pagina "Campeonatos". El 3er puesto de cada grupo recibe 50 Puntos y el 4to puesto recibe 30 Puntos</p>
    </div>
</div>
@endsection