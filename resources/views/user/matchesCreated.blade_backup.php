@extends('layouts.app')

@section('content')
<div class="limiter">
    <h1>Partidos Creados</h1>
    <div class="container d-flex flex-wrap">
        @foreach ($matches_created as $match)
            <div class="card border-primary mb-3" style="max-width: 18rem;">
                <div class="card-header">{{$match->date}}</div>
                <div class="card-body">
                    <h5 class="card-title">You! V.S {{$match->player2->names}}</h5>
                    @foreach ($match->sets as $set)
                        <p class="card-text">Set N°{{$set->number}}</p>
                        <p class="card-text">You {{$set->player1}} | {{$match->player2->names}} {{$set->player2}}</p>
                    @endforeach
                </div>
                <a class="btn btn-primary" href="{{url("user/$match->id/editMatch")}}">Editar</a>
                <form action="{{ route('user.deleteMatch', $match->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger container">Eliminar</button>
                </form>
            </div>
        @endforeach
    </div>
</div>
@endsection