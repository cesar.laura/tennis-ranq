@extends('layouts.app')

@section('content')
<div class="limiter matches">
    <h1>Partidos Creados</h1>

    <div class="search-boxen d-flex">
        
        <div class="search-result">
            @if (count($matches_created))
                @foreach ($matches_created as $match)
                <div class="benchmarks table matchesCreated">
                    <ul class="table-body"> 
                        <li class="tr">
                            <span class="metric td" style="width: 100px;">{{$match->player1->names}}<label> </label></span>
                            @foreach ($match->sets as $set)
                            <span class="td percentile-76"><span class="number">{{$set->player1}}</span></span>
                            @endforeach   
                        </li>
                        <li class="tr">
                            <span class="metric td">{{$match->player2->names}} <label></label></span>
                            @foreach ($match->sets as $set)
                            <span class="td percentile-67"><span class="number">{{$set->player2}}</span></span>
                            @endforeach  
                        </li>
                    </ul>
                    <ul class="table-footer">
                        <li class="th"></li>
                        <li class="th"><label>{{$match->date}}</label></li>
                        <li class="th"></li>
                    </ul>
                    <ul class="table-footer editar">
                        <a class="btn btn-primary" href="{{url("user/$match->id/editMatch")}}">EDITAR</a>
                        <form action="{{ route('user.deleteMatch', $match->id) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger" value="Eliminar">
                            
                        </form>
                    </ul>               
                </div>
                @endforeach
            @else
                <h2>No tiene partidos creados</h2>    
                <div class="container d-flex flex-wrap">
                    <a href="{{ route('user.createMatch') }}" class="login100-form-btn">Crear Uno</a>            
                <div class="card border-primary mb-3" style="max-width: 18rem;">
            @endif
        </div>
    </div>       
</div>
@endsection
