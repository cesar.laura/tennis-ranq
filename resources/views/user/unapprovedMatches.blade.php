@extends('layouts.app')

@section('content')
<div class="limiter matches">
    <h1>Partidos sin aprobar</h1>

    

        <div class="search-boxen d-flex">
            
            <div class="search-result">
                @if (count($unapproved_matches))
                    @foreach ($unapproved_matches as $match)
                    <div class="benchmarks table matchesCreated">
                        <ul class="table-body"> 
                            <li class="tr">
                                <span class="metric td" style="width: 100px;">{{$match->player2->names}}<label> </label></span>
                                @foreach ($match->sets as $set)
                                <span class="td percentile-76"><span class="number">{{$set->player2}}</span></span>
                                @endforeach   
                                
                            </li>
                            <li class="tr">
                                <span class="metric td">{{$match->player1->names}} <label></label></span>
                                @foreach ($match->sets as $set)
                                <span class="td percentile-67"><span class="number">{{$set->player1}}</span></span>
                                @endforeach  
                            </li>
                        
                        </ul>
                        <ul class="table-footer">
                            <li class="th"></li>
                            <li class="th"><label>{{$match->date}}</label></li>
                            <li class="th"></li>
                        </ul>
                        <ul class="table-footer btn-footer">
                            <form action="{{url("user/$match->id/editMatch")}}" method="POST">
                                @csrf
                                @method('PUT')
                                <input type="submit" class="btn-update" value="Aprobar">
                               <!-- <button type="submit" class="btn-success">Aprobar</button~>-->
                            </form>
                        </ul>               

                    </div>
                    @endforeach
                @else
                    <h2>No hay partidos por aprobar</h2>
                @endif
            </div>
            
        </div>       
    
</div>
@endsection
