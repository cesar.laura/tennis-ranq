@extends('layouts.app')

@section('content')
<div class="limiter">
    <div class="card border-primary mb-3" style="max-width: 18rem;">
        <div class="card-header">{{Auth::user()->information->points}}</div>
        <div class="card-body">
            <h5 class="card-title">Number of wins: {{Auth::user()->information->number_of_wins}}</h5>
            <h5 class="card-title">Number of defeats: {{Auth::user()->information->number_of_defeats}}</h5>
        </div>
    </div>

    @if(count($matches))
        <a class="btn btn-primary" href="{{route('user.matches')}}">Partidos Publicados</a>
    @else
        <p class="btn btn-dark">No tienes Partidos Publicados</p>
    @endif
    <hr>
    @if(count($matches_created))
        <a class="btn btn-primary" href="{{route('user.matchesCreated')}}">Partidos Creados</a>
    @else
        <a class="btn btn-success" href="{{route('user.createMatch')}}">Crear Uno!</a>
    @endif
    <hr>
    @if(count($unapproved_matches))
        <a class="btn btn-primary" href="{{route('user.unapprovedMatches')}}">Partidos sin aprobar</a>
    @else
        <p class="m-4">Listo!</p>
    @endif
</div>
@endsection
