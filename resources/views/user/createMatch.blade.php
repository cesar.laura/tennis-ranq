@extends('layouts.app')

@section('content')

        <div class="plans-main">
       
                <form class = "formulario" action="{{route('user.store')}}" method="POST">
                    @csrf
                    <div class="price-grid lost">
                        <div class="price-block price-block2 agile">

                            <div class="price-gd-top pric-clr3">
                                <i class="fa fa-diamond" aria-hidden="true"></i>
                                <h4>Crear Nueva Partida</h4>
                                <div class="wrapper">
                                    <label class="clabel">
                                        <input type="date" value="{{$today_date}}" name="date" class="dateselect" />
                                        <span>Fecha</span>
                                    </label>
                                    <label class="clabel">
                                        <h3>Rival</h3>
                                        <select name="email" class="custom-select">
                                            @foreach ($users as $user)
                                                <option  value="{{$user->email}}">{{$user->names}} {{$user->surnames}}</option>
                                            @endforeach
                                        </select>
                                        
                                    </label>
                                    <label class="clabel">
                                        <input type="number" class="dateselect2" min="1" max="3"  name="number_of_sets" required="required"  >
                                        <span>Sets</span>
                                    </label>
                                </div>
                            </div>
                            <div class="price-gd-bottom">
                                <div class="price-list">
                                </div>
                                <div class="price-selet pric-sclr3">
                                 <!--<a href="#small-dialog" class="w3_agileits_sign_up2 popup-with-zoom-anim  ab scroll" >Signup Now</a>-->
                                 <input type="submit" class="w3_agileits_sign_up2 popup-with-zoom-anim  ab scroll a" value="Crear">
                                </div>
                            </div>
                        </div>
                    </div>	
                </form> 

        </div>
      


@endsection
