@extends('layouts.app')

@section('content')
<div class="limiter matches">
    <h1>{{$match->player1->names}} vs {{$match->player2->names}}</h1>
    
    <form class="formulario" action="{{url("user/$match->id/editMatch")}}" method="POST">
        @csrf
        @method('PUT')
        <div class="search-boxen d-flex">
            <div class="search-result">
             
                    <div class="benchmarks table editMatches">
                        <ul class="table-body"> 
                            <li class="tr">
                                <span class="metric td" style="width: 100px;">{{$match->player1->names}}<label> </label></span>
                               @foreach ($match->sets as $set)
                               <input type="number" min="0" max="10" name="set_points[]" value="{{$set->player1}}" placeholder="Player 1">
                               <!-- <span class="td percentile-76"><span class="number">{{$set->player2}}</span></span>-->
                                @endforeach   
                            </li>
                            <li class="tr">
                                <span class="metric td" style="width: 100px;">{{$match->player2->names}}<label> </label></span>
                               @foreach ($match->sets as $set)
                               <input type="number" min="0" max="10"  name="set_points[]" value="{{$set->player2}}" placeholder="Player 2">
                               <!-- <span class="td percentile-76"><span class="number">{{$set->player2}}</span></span>-->
                                @endforeach   
                            </li>
                        
                        </ul>
                        <ul class="table-footer">
                            <li class="th"></li>
                            <li class="th"><label>{{$match->date}}</label></li>
                            <li class="th"></li>
                        </ul>
                        <ul class="table-footer btn-footer">
                                <input type="submit" class="btn btn-update" value="Actualizar">
                
                        </ul>               

                    </div>
                  

            </div>
            
        </div>       
    </form>
</div>
@endsection
