@extends('layouts.app')

@section('content')
<div class="limiter">
    <form class="formulario" action="{{url("user/$match->id/editMatch")}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group row">
            <div class="col-md-6">
                <input type="text" readonly class="form-control" name="date" value="{{$match->player1->names}} {{$match->player1->surnames}}">
                <p><b>V.S</b></p>
                <input type="text" readonly class="form-control" name="date" value="{{$match->player2->names}} {{$match->player2->surnames}}">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <input type="text" readonly class="form-control" name="date" value="{{$match->date}}">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                <input readonly type="text" class="form-control" name="number_of_sets" value="{{$match->number_of_sets}}">
            </div>
        </div>
        <div class="form-group row mb-0">
            @foreach ($match->sets as $set)
                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <label>Set N° {{$set->number}}</label>
                        <input type="number" min="0" max="10" name="set_points[]" value="{{$set->player1}}" placeholder="Player 1">
                        <input type="number" min="0" max="10" name="set_points[]" value="{{$set->player2}}" placeholder="Player 2">
                    </div>
                </div>
            @endforeach
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-8 offset-md-4">
                <!--<button type="submit" class="btn btn-primary">Actualizar</button>-->
                <input type="submit" class="btn btn-primary" value="Actualizar">
            </div>
        </div>
    </form>
<div class="limiter">

@endsection
