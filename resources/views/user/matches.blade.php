@extends('layouts.app')

@section('content')
<div class="limiter">
    <h1>Tus Partidos Publicados</h1>
    <div class="search-boxen d-flex">
       @if (count($matches))
            @foreach ($matches as $match)
            <div class="search-result">
                <div class="benchmarks table matchesCreated">
                    <ul class="table-body"> 
                        <li class="tr">
                            <span class="metric td" style="width: 100px;">{{$match->player1->names}}<label> </label></span>
                            @foreach ($match->sets as $set)
                                <span class="td percentile-76"><span class="number">{{$set->player1}}</span></span>
                            @endforeach   
                        </li>
                        <li class="tr">
                            <span class="metric td">{{$match->player2->names}} <label></label></span>
                            @foreach ($match->sets as $set)
                            <span class="td percentile-67"><span class="number">{{$set->player2}}</span></span>
                            @endforeach  
                        </li>
                    </ul>
                    <ul class="table-footer">
                        <li class="th"></li>
                        <li class="th"><label>{{$match->date}}</label></li>
                        <li class="th"></li>
                    </ul>
                </div>
            </div>
            @endforeach
       @else
           <h2>No tienes partidos publicados</h2>
       @endif
    </div>   
</div>    
@endsection