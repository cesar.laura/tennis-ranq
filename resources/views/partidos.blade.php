@extends('layouts.app')

@section('content')
<div class="limiter">
<h1>Partidos - Al 15 Marzo</h1>
    <div class="search-boxen d-flex">
    <div class="search-result">
      <div class="benchmarks table">
        <!--
        <ul class="table-header">
          <li class="th"></li>
          <li class="th"><label>Set 1</label></li>
          <li class="th"><label>Set 2</label></li>
          <li class="th"><label>Set 3</label></li>
        </ul>-->
        
        <ul class="table-body"> 
          <li class="tr">
            <span class="metric td" style="width: 6%;">Cesar<label> Laura</label></span>
            <span class="td percentile-76"><span class="number">6</span></span>
            <span class="td percentile-87"><span class="number">5</span></span>
            <span class="td percentile-32"><span class="number">6</span></span>
          </li>
          <li class="tr">
            <span class="metric td">Derry <label>Sucari</label></span>
            <span class="td percentile-67"><span class="number">4</span></span>
            <span class="td percentile-98"><span class="number">6</span></span>
            <span class="td percentile-42"><span class="number">1</span></span>
          </li>
    
        </ul>
        <ul class="table-footer">
          <li class="th"></li>
          <li class="th"><label>2/20/2020</label></li>
          <li class="th"></li>
        </ul>
        
      </div>
      <div class="benchmarks table">
        <!--
        <ul class="table-header">
          <li class="th"></li>
          <li class="th"><label>Set 1</label></li>
          <li class="th"><label>Set 2</label></li>
          <li class="th"><label>Set 3</label></li>
        </ul>-->
        
        <ul class="table-body"> 
          <li class="tr">
            <span class="metric td" style="width: 100px;">Cesar <label> Laura</label></span>
            <span class="td percentile-76"><span class="number">6</span></span>
            <span class="td percentile-87"><span class="number">5</span></span>
            <span class="td percentile-32"><span class="number">6</span></span>
          </li>
          <li class="tr">
            <span class="metric td">Derry <label>Sucari</label></span>
            <span class="td percentile-67"><span class="number">4</span></span>
            <span class="td percentile-98"><span class="number">6</span></span>
            <span class="td percentile-42"><span class="number">1</span></span>
          </li>
    
        </ul>
        <ul class="table-footer">
          <li class="th"></li>
          <li class="th"><label>2/20/2020</label></li>
          <li class="th"></li>
        </ul>
      </div>
      <div class="benchmarks table">
        <!--
        <ul class="table-header">
          <li class="th"></li>
          <li class="th"><label>Set 1</label></li>
          <li class="th"><label>Set 2</label></li>
          <li class="th"><label>Set 3</label></li>
        </ul>-->
        
        <ul class="table-body"> 
          <li class="tr">
            <span class="metric td" style="width: 100px;">Cesar <label> Laura</label></span>
            <span class="td percentile-76"><span class="number">6</span></span>
            <span class="td percentile-87"><span class="number">5</span></span>
            <span class="td percentile-32"><span class="number">6</span></span>
          </li>
          <li class="tr">
            <span class="metric td">Derry <label>Sucari</label></span>
            <span class="td percentile-67"><span class="number">4</span></span>
            <span class="td percentile-98"><span class="number">6</span></span>
            <span class="td percentile-42"><span class="number">1</span></span>
          </li>
    
        </ul>
        <ul class="table-footer">
          <li class="th"></li>
          <li class="th"><label>2/20/2020</label></li>
          <li class="th"></li>
        </ul>
      </div>
    </div>
    </div>
 </div>         
@endsection
