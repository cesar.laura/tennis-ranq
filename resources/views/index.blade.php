@extends('layouts.app')

@section('content')



<div class="limiter">
    <h1>Ranking - Al 15 Marzo</h1>

                    <div class="container-table100">
                        <div class="wrap-table100">
                            <div class="table100">
                                <table class= "table-rank">
                                    <thead class = "table-head">
                                        <tr class="table100-head">
                                            <th class="column1">Rank</th>
                                            <th class="column2">Jugador</th>
                                            <th class="column3">Puntos</th>
                                            <th class="column4">+/-</th>


                                        </tr>
                                    </thead>
                                    <tbody class = "table-tbody">
                                    <?php $aux = 1;?>
                                    @foreach ($informations as $information)
                                        <tr>
                                            <td class="column1">{{$aux}}</td>
                                            <td class="column2">{{$information->user->names}} {{$information->user->surnames}}</td>
                                            <td class="column3">{{$information->points}}</td>
                                            <td class="column4 pointsWL">{{$information->last_points_won}}</td>
                                        </tr>
                                        <?php $aux ++;?>
                                    @endforeach
                                                                                                                                                                             
                                    </tbody>
                               </table>
                            </div>
                        </div>
                    </div>
            

</div>




@endsection
