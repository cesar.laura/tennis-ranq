@extends('layouts.app')

@section('content')
<div class="limiter">

<div class="bottom-champion">
        <h1>Criterio</h1>
        <p>Todos los jugadores inician en el Ranking con 200 puntos.</p>
        <p> Los Campeonatos entregan puntos según la ronda en la que 
            queda cada jugador. Al siguiente año, estos puntos se pierden 
            y son reemplazados por los puntos del torneo equivalente.</p>
        <p> ​​Los partidos amistosos o desafíos son pactados por los
             jugadores según su disponibilidad. Se debe acordar el 
             tipo de partido que permita tener un ganador (a 1 set, 
             2 sets, set de 8 games, etc). </p>
        <p> ​​Los partidos amistosos o desafíos entregan puntos de 
            acuerdo a la cantidad de puntos que tengan los jugadores 
            que se enfrentan según la siguiente fórmula:</p>
    </div>
    <div class="evidencia-criterio">
    
      <img src="{{ asset('images/Formula.png')}}" style="width:90%">

    </div>
    <div class="bottom-champion">
        <p>Los resultados de los partidos amistosos o desafios tienen
             que ser aceptados por ambos jugadores y se comunican por el 
             chat del grupo. Los partidos de la semana estarán publicados
              en la sección "Partidos" de esta página.</p>
        <p> ​​El Ranking se actualiza semanalmente y se promedia la cantidad 
            de puntos obtenidos en los partidos de la semana (Lun - Dom). 
            Adicionalmente, se bonifica con 2 puntos a los jugadores que 
            hayan disputado al menos un partido en la semana.</p>
        <p> ​​Los jugadores que no tengan partidos registrados en 6 semanas,
             perderán 15 puntos en el ranking.</p>           
    </div>
</div>
@endsection