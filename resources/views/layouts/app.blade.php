<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <!--script src="{{ asset('js/app.js') }}" defer></script-->

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/estilo1.css') }}" rel="stylesheet">
    <link href = "{{ asset('css/partidos.css') }}" rel="stylesheet">
    <link href = "{{ asset('css/matchesCreated.css') }}" rel="stylesheet">
    <link href = "{{ asset('css/style-estadisticas.css') }}" rel="stylesheet">
    <link href = "{{ asset('css/unapproved.css') }}" rel="stylesheet">
    <link href = "{{ asset('css/matches.css') }}" rel="stylesheet">
    <link href = "{{ asset('css/edit.css') }}" rel="stylesheet">
    <link href = "{{ asset('css/style.css') }}" rel="stylesheet">
    
    
    <script src="https://kit.fontawesome.com/0458944bda.js" crossorigin="anonymous"></script>
</head>
<body >
<header class="header">
            <div class="header-top">
                <div class="container-header logo-nav-container">
                    <!--<img src="../logo.png" class="logo">-->
                    <div href="{{ URL::route('index') }}" class="logo"><img src = "{{ asset('logowhite.png') }}"></div>
                    
                    
                    <div class="menu-btn">
                        <div class="menu-btn__burger"></div>
                    </div>

                    <nav class="navigation">
                        <ul class="show">
                            <li><a href="{{ URL::route('index') }}">RANKING</a></li>
                            <li><a href= "{{ URL::route('matches') }}" >PARTIDOS</a></li>
                            <li><a href= "{{ URL::route('statistics') }}"  >ESTADÍSTICAS</a></li>
                            <li><a href="{{ URL::route('criterion') }}">CRITERIOS</a></li>
                            <li><a href="{{ URL::route('championship') }}">CAMPEONATOS</a></li>
                            <li><a href="{{ URL::route('gallery') }}">FOTOS</a></li>
                            @guest
                                <li><a href= "{{ URL::route('login') }}" >INGRESAR</a></li>
                            @endguest
                         </ul> 
                         @auth   
                            <li class="submenu ">
                                <a href="#">&#9679;&nbsp;{{ Auth::user()->names }}<span class="caret icon-arrow-down6"></span></a>
                                <ul class="children">
                                    <li><a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">{{ __('Logout') }}<span class="icon-dot"></span></a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                    @csrf
                                                </form></li>
                                    <li><a href="{{route('user.createMatch')}}">Crear Partido<span class="icon-dot"></span></a></li>
                                    <li><a href="{{route('user.matchesCreated')}}">Partidos Pendientes<span class="icon-dot"></span></a></li>
                                    <li><a href="{{route('user.matches')}}">Mis Partidas<span class="icon-dot"></span></a></li>
                                    <li><a href="{{route('user.unapprovedMatches')}}">Aprobar Partidos<span class="icon-dot"></span></a></li>
                                </ul>
                            </li>          
                            @endauth                         
                    </nav>
                </div>
            </div>    
            <div class="header-mid"></div>
            
            <div class="header-bottom">
                <!--<img src="../logo.png" class="logo">-->
                <div  class="text-hb">Los Leones - El Tenis entre amigos</div>
            </div>
            
        </header>

        <main class="main">
            <div class="top-main"></div>
            <div class="container">
                @auth
                <div class="container justify-content-center">
                    <div class="row container justify-content-center">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="alert alert-success text-center font-weight-bold">
                               <!-- {{ Auth::user()->names }} Variavle flash-->
                            </div>
                        </div>
                    </div>
                </div>
                @endauth
                @yield('content')
            </div>
        </main>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> 
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js'></script>
    <script  src="{{asset('js/script.js')}}"></script>
    <script src="{{ asset('js/javaS1.js') }}"></script>
    <script  src="{{asset('js/nav-btn.js')}}"></script>
    <script  src="{{asset('js/submenu.js')}}"></script>
    <script  src="{{asset('js/main.js')}}"></script>
    <script  src="{{asset('js/script-galeria.js')}}"></script>
</body>
</html>
